��          �   %   �      P  7   Q  
   �  4   �     �  !   �  ;   �  P   5  B   �  Y   �  U   #  I   y  d   �  8   (  \   a  %   �     �     �  �   �  Z   �  )   �  ,     1   2  )   d  `   �  \   �  �  L  5   
  
   S
  ,   ^
     �
      �
  =   �
  D   �
  L   =  `   �  e   �  X   Q  j   �  7     T   M  6   �     �  
   �  �   �  N   }  :   �  /     A   7  %   y  Z   �  i   �                                	                                                                             
                  GUI tool for safely unplugging removable USB devices.   Options:   Questions, Suggestions and Bugreporting please to:   Unplugdrive   Usage: unplugdrive.sh [options] <b>Aborting</b> on user request\n<b>without unmounting.</b> <b>About to unmount:</b>\n$summarylist<b>Please confirm you wish to proceed.</b> <b>Check each mountpoint listed before unpluging the drive(s).</b> <b>Mounted USB partitions:</b>\n$removablelist<b>Choose the drive(s) to be unplugged:</b> <b>Mountpoint removal failed.</b>\n<u>One or more mountpoin(s) remain present at:</u> <b>Unmounted:</b>\n$summarylist\n<b>It is safe to unplug the drive(s)</b> A removable drive with a mounted\npartition was not found.\n<b>It is safe to unplug the drive(s)</b> Data is being written\nto devices. <b>Please wait...</b> Invalid command line argument. Please call\nunplugdrive -h or --help for usage instructions. Taskbar icon $icon_taskbar not found. Unplug USB device Version  \033[1;31mWARNING: DEVICE ${deviceslist[u]} WAS NOT PROPERLY UNMOUNTED!\033[0m\n\033[4;37mPlease check before unplugging.\033[0m \nˮyadˮ is not installed.\n   --> Please install ˮyadˮ before executing this script.\n \t-c or --centred\t\topen dialogs centred \t-d or --decorated\tuses window decorations \t-g or --debug\t\tenable debug output (terminal) \t-h or --help\t\tdisplays this help text \t-p or --pretend\t\tdry run, don't actually un-\n\t            \t\tmount drives (for debugging) \t-s or --safe\t\truns script in safe mode,\n\t            \t\tdisplaying some extra dialogs Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2011-10-27 11:12+0000
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2019-2021
Language-Team: Slovenian (http://www.transifex.com/anticapitalista/antix-development/language/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 Grafični vmesnik za varno odstranjevanje USB naprav. Možnosti: Vprašanja, predloge in napake pošljite na:   Unplugdrive Raba: unplugdrive.sh [možnosti] <b>Prekinitev</b> na zahtevo uporabnika\n<b>brez odklopa.</b> <b>Pred odklopom:</b>\n$summarylist<b>Potrdite izvedbo postopka.</b> <b>Preden odklopite napravo (naprave) preverite seznam priklopnih točk.</b> <b>Priklopljeni USB razdelki</b>\n$removablelist<b>Izberite pogone, ki naj bodo odstranjeni:</b> <b>Priklopne točke ni bilo mogoče odstraniti.</b>\n<u>Ena ali več priklopnih točka ostaja na:</u> <b>Odklopljeno:</b>\n$summarylist\n<b>Nosilec (nosilce) JE MOGOČE VARNO ODSTRANITI</b>  Odkrit ni bil noben pogon s priklopljenim razdelkom.\n<b>Nosilec (nosilce) je mogoče varno odstraniti</b> Na napravo se zapisujejo\npodatki. <b>Počakajte...</b> Napačen argument ukazne vrstice. Uporabite \nunplugdrive -h ali --help za navodila. Ikona opravilne vrstice $icon_taskbar ni bila najdena. Odstranite USB napravo Različica \033[1;31mOPOZORILO: NAPRAVA ${deviceslist[u]} NI BILA PRAVILNO ODKLOPLJENA!\033[0m\n\033[4;37mPred odklopom jo preverite.\033[0m \nˮyadˮ ni namščen.\n   --> Namestite ˮyadˮ pred zagonom tega skripta.\n \t-c ALI --centred\t\srednja poravnava odpiralnih dialogov \t-d ali --decorated\tuporablja okraske za okna \t-g ali --debug\t\tvklopi izpis razhroščevalnika (v terminalu) \t-h ali --help\t\tprikaže to pomoč \t-p ali --pretend\t\tpreizkusni zagon, ki ne odklopi pogonov\n\t\t\ (za razhroščevanje) \t-s ali --safe\t\tzažene skript v varnem načinu,\n\t            \t\ts prikazom nekaj dodatnih dialogov 